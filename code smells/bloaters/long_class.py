# ------------------------------ ONE HUMONGOUS NIGHTMARISH CLASS -------------------------------------------
class StorageTable:

    def __init__(self):
        pass

    def create(self):
        pass

    def get_columns(self):
        pass

    def connect(self):
        pass

    def drop(self):
        pass

    def read_data(self):
        pass

    def get_size(self):
        pass

    def get_column_values(self, column):
        pass

    def join(self, *args, **kwargs):
        pass

    def create_column(self, *args, **kwargs):
        pass

    def replace_values(self, *args, **kwargs):
        pass

    def merge_columns(self, *args, **kwargs):
        pass

    def split_column(self, *args, **kwargs):
        pass

    def append_table(self, *args, **kwargs):
        pass

    def transpose(self, *args, **kwargs):
        pass


# ----------------- APPROACH 1: INDEPENDENT CLASSES WITH DEPENDENCY INJECTION  -----------------

class StorageTable:

    def __init__(self):
        pass

    def create(self):
        pass

    def get_columns(self):
        pass

    def connect(self):
        pass

    def drop(self):
        pass

    def read_data(self):
        pass

    def get_size(self):
        pass

    def get_column_values(self, column):
        pass


class StorageTableOperations:

    def __init__(self, storage_table):
        self.storage_table = storage_table

    def join(self, *args, **kwargs):
        pass

    def create_column(self, *args, **kwargs):
        pass

    def replace_values(self, *args, **kwargs):
        pass

    def merge_columns(self, *args, **kwargs):
        pass

    def split_column(self, *args, **kwargs):
        pass

    def append_table(self, *args, **kwargs):
        pass

    def transpose(self, *args, **kwargs):
        pass


# ----------------------- APPROACH 2: COMPOSITION WITH DELEGATION  ----------------------------------

class StorageTable:

    def __init__(self):
        self.operator = StorageTableOperations()
        pass

    def create(self):
        pass

    def get_columns(self):
        pass

    def connect(self):
        pass

    def drop(self):
        pass

    def read_data(self):
        pass

    def get_size(self):
        pass

    def get_column_values(self, column):
        pass

    # THESE ARE THE VERY USEFUL DELEGATOR METHODS

    def join(self, *args, **kwargs):
        self.operator.join(self, *args, **kwargs)

    def create_column(self, *args, **kwargs):
        self.operator.create_column(self, *args, **kwargs)

    def replace_values(self, *args, **kwargs):
        self.operator.replace_values(self, *args, **kwargs)

    def merge_columns(self, *args, **kwargs):
        self.operator.merge_columns(self, *args, **kwargs)

    def split_column(self, *args, **kwargs):
        self.operator.split_column(self, *args, **kwargs)

    def append_table(self, *args, **kwargs):
        self.operator.append_table(self, *args, **kwargs)

    def transpose(self, *args, **kwargs):
        self.operator.transpose(self, *args, **kwargs)


class StorageTableOperations:

    def __init__(self, storage_table):
        self.storage_table = storage_table

    def join(self, *args, **kwargs):
        pass

    def create_column(self, *args, **kwargs):
        pass

    def replace_values(self, *args, **kwargs):
        pass

    def merge_columns(self, *args, **kwargs):
        pass

    def split_column(self, *args, **kwargs):
        pass

    def append_table(self, *args, **kwargs):
        pass

    def transpose(self, *args, **kwargs):
        pass
