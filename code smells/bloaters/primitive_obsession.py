# ---------------------------- Example 1 ------------------------------------

"""
 Storing a lot of details in primitive data types, hampers readability and difficult to change,
 hence unmaintainable and fragile.
 """


class Person:

    def __init__(self, **kwargs):
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.age = kwargs.get('age')
        self.country = kwargs.get('country')
        self.state = kwargs.get('state')
        self.city = kwargs.get('city')
        self.town = kwargs.get('town')
        self.locality = kwargs.get('locality')
        self.employer = kwargs.get('employer')
        self.salary = kwargs.get('salary')
        self.manager = kwargs.get('manager')
        self.role = kwargs.get('role')


"""
 Clubbing details into modules, this improves readability and makes code easy to change
 """


class Person:

    def __init__(self, **kwargs):
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.age = kwargs.get('age')
        self.address = Address(kwargs)
        self.employment_info = EmploymentInformation(kwargs)


class Address:
    def __init__(self, **kwargs):
        self.country = kwargs.get('country')
        self.state = kwargs.get('state')
        self.city = kwargs.get('city')
        self.town = kwargs.get('town')
        self.locality = kwargs.get('locality')


class EmploymentInformation:
    def __init__(self, **kwargs):
        self.employer = kwargs.get('employer')
        self.salary = kwargs.get('salary')
        self.manager = kwargs.get('manager')
        self.role = kwargs.get('role')


# ---------------------------- Example 2 ------------------------------------
"""
  Storing coding information such as user with ID is the Admin is a painful practice and 
  is rigid to change. Makes the function non-reusable 
"""


def assign_admin_privileges():
    ADMIN = 1
    # Some stuff about giving privileges to the user with ID - 1,
    pass


"""
  This is also known as dependency injection. Whatever you need to run a function is passed to the
    function rather than assuming it has it already. This decouples the function 
    and makes it highly reusable 
"""


def assign_admin_privileges(admin):
    # Some stuff about giving privileges to the user whose ID is provided
    pass
