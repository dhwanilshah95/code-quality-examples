# ------------------------------ ONE LONG UGLY METHOD -------------------------------------------


def get_trend_chart(data):
    return {
        'tooltip': {
            'trigger': 'axis',
            'transitionDuration': 0.2,
            'confine': True,
            'borderWidth': 0,
            'borderColor': 'white',
            'backgroundColor': None,
            'formatter': """function(params) {
        let {confirmed, deaths, recovered} = {
          'confirmed': params[0], 'deaths': params[1],
          'recovered': params[2]
        };

        return `< div class = "map-tooltip" > <h4 >${params[0].name} < /h4 >
                < b class = "confirmed-tag" > Confirmed < /b >: ${confirmed.value | | '-'} < br/>
                < b class = "recovered-tag" > Recovered < /b >: ${recovered.value | | '-'} < br/>
                < b class = "dead-tag" > Deaths < /b > : ${deaths.value | | '-'}
                < /div >`;}"""
        },
        'series': {
            'type': 'line',
            'data': data,
            'lineStyle': {
                'color': 'red',
                'width': 3,
            },
            'itemStyle': {
                'color': 'red',
                'borderWidth': 3,
            }
        },
        'xAxis': {
            'type': 'category',
            'boundaryGap': False,
            'data': data,
            'axisLabel': {
                'fontSize': 14,
                'rotate': 45
            }
        },
        'yAxis': {
            type: 'value'
        },
        'legend': {
            'data': data,
            'show': True,
            'top': '40',
            'left': 'center',
            'itemGap': 15,
            'textStyle': {
                'fontSize': 14,
            }
        },
        'grid': {
            'left': '5%',
            'right': '5%',
            'top': '15%',
            'bottom': '10%',
            'containLabel': True
        },
        'title': {
            'text': 'COVID-19 Spread Overtime (India)',
            'show': True,
            'left': 'center',
            'top': 5,
            'padding': 10,
            'textStyle': {
                'fontSize': 18
            }
        },
        'dataZoom': [{
            'startValue': data['dates'][0],
            'height': 20,
            'left': 'center',
            'width': '70%'
        }, {
            'type': 'inside'
        }]
    }


# ------------------------------ MULTIPLE SHORT MANAGEABLE METHODS -------------------------------------------


def get_trend_chart(data):
    return {
        'tooltip': get_tooltip(),
        'series': get_series(data['trend']),
        'xAxis': get_x_axis(data['dates']),
        'yAxis': get_y_axis(),
        'legend': get_legend(),
        'grid': get_grid(),
        'title': get_title(),
        'dataZoom': get_data_zoom(data),
    }


def get_tooltip():
    return {
        'trigger': 'axis',
        'transitionDuration': 0.2,
        'confine': True,
        'borderWidth': 0,
        'borderColor': 'white',
        'backgroundColor': None,
        'formatter': """function(params) {
      let {confirmed, deaths, recovered} = {
        'confirmed': params[0], 'deaths': params[1],
        'recovered': params[2]
      };

      return `< div class = "map-tooltip" > <h4 >${params[0].name} < /h4 >
              < b class = "confirmed-tag" > Confirmed < /b >: ${confirmed.value | | '-'} < br/>
              < b class = "recovered-tag" > Recovered < /b >: ${recovered.value | | '-'} < br/>
              < b class = "dead-tag" > Deaths < /b > : ${deaths.value | | '-'}
              < /div >`;}"""
    }


def get_series(data):
    return {
        'type': 'line',
        'data': data,
        'lineStyle': {
            'color': 'red',
            'width': 3,
        },
        'itemStyle': {
            'color': 'red',
            'borderWidth': 3,
        }
    }


def get_x_axis(data):
    return {
        'type': 'category',
        'boundaryGap': False,
        'data': data,
        'axisLabel': {
            'fontSize': 14,
            'rotate': 45
        }
    }


def get_y_axis():
    return {
        type: 'value'
    }


def get_legend(data):
    return {
        'data': data,
        'show': True,
        'top': '40',
        'left': 'center',
        'itemGap': 15,
        'textStyle': {
            'fontSize': 14,
        }
    }


def get_title():
    return {
        'text': 'COVID-19 Spread Overtime (India)',
        'show': True,
        'left': 'center',
        'top': 5,
        'padding': 10,
        'textStyle': {
            'fontSize': 18
        }
    }


def get_grid():
    return {
        'left': '5%',
        'right': '5%',
        'top': '15%',
        'bottom': '10%',
        'containLabel': True
    }


def get_data_zoom(data):
    return [{
        'startValue': data['dates'][0],
        'height': 20,
        'left': 'center',
        'width': '70%'
    }, {
        'type': 'inside'
    }]
