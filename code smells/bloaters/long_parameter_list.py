# --------------------------- PROBLEM ---------------------------
class OperationAPIView:

    def post(self, request, **kwargs):
        data = request.data
        execute_operation(data['name'], data['configuration'],
                          data['id'], data['preview'], **kwargs)


# Assume multiple other params are passed
def execute_operation(name, configuration, id, preview, **kwargs):
    pass

# --------------------------- Preserve whole object ---------------------------


class OperationAPIView:

    def post(self, request, **kwargs):
        execute_operation(request.data, **kwargs)


# Assume multiple other params are passed
def execute_operation(operation, **kwargs):
    pass


# --------------------------- PROBLEM ---------------------------
class POS:

    def process(self, product):
        # Step 1: Update Inventory
        # Step 2: Update Bill
        base_price = product.base_price
        seasonal_discount = self.get_discount()
        charges = self.get_additional_charges()
        tax = self.get_taxes(base_price)
        final_price = self.get_final_price(
            base_price, seasonal_discount, charges, tax)

    def get_final_price(self, base_price, seasonal_discount, charges, tax):
        # Calculate and return final price
        return (base_price * seasonal_discount) + tax + charges
        # --------------------------- Replace params with method calls (SOLUTION) ---------------------------


class POS:

    def process(self, product):
        # Step 1: Update Inventory
        # Step 2: Update Bill
        final_price = self.get_final_price(product)

    def get_final_price(self, product):
        seasonal_discount = self.get_discount()
        charges = self.get_additional_charges()
        tax = self.get_taxes(product.base_price)
        return (product.base_price * seasonal_discount) + tax + charges

        # Calculate and return final price
