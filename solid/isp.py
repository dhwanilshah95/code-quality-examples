"""
ISP - Interface Segregation Principle

A client should never be forced to implement an interface that it doesn't use or
 clients shouldn't be forced to depend on methods they do not use.

"""

# ------------------------------ Example 1 -------------------------------------

# ------------------------------ DON'T -------------------------------------
from abc import ABC, abstractmethod


class Pet(ABC):

    @abstractmethod
    def feed(self):
        pass

    @abstractmethod
    def clean(self):
        pass

    @abstractmethod
    def pet(self):
        pass

    @abstractmethod
    def cuddle(self):
        pass

    @abstractmethod
    def take_for_walk(self):
        pass


class Dog(Pet):

    def feed(self):
        # Obviously who says no to food?
        pass

    def clean(self):
        # Might get cranky but will still work
        pass

    def pet(self):
        # Craving for that anytime and anyplace
        pass

    def cuddle(self):
        # Will do even if you are working
        pass

    def take_for_walk(self):
        # No need to ask twice
        pass


class Cat(Pet):

    def feed(self):
        # Finally hooman, don't make me wait the next time
        pass

    def clean(self):
        # You might end but with a lot of scratches but it is needed
        pass

    def pet(self):
        # Always gives double signals, why so confused man?
        pass

    def cuddle(self):
        # Again, indecisive creatures!
        pass

    def take_for_walk(self):
        # No need, let me sleep peacefully
        pass


class Alligator(Pet):
    def feed(self):
        # Be careful, be alert
        pass

    def clean(self):
        # Only if you have death wish
        pass

    def pet(self):
        # You might lose an arm, but who cares
        pass

    def cuddle(self):
        # Rest In Peace brother, you had a good run but you asked for this!
        pass

    def take_for_walk(self):
        # Only if you want other people to get you arrested
        pass


# ------------------------------ DON'T -------------------------------------

class NormalPet(ABC):

    @abstractmethod
    def clean(self):
        pass

    @abstractmethod
    def pet(self):
        pass

    @abstractmethod
    def cuddle(self):
        pass

    @abstractmethod
    def take_for_walk(self):
        pass


class KillerPet(ABC):

    @abstractmethod
    def feed(self):
        pass


class Dog(NormalPet):
    pass


class Cat(NormalPet):
    pass


class Alligator(KillerPet):
    pass

