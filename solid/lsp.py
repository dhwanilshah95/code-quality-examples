"""
LSP - Liskov Substitution Principle

Derived class must be usable through the base class interface,
without the need for the user to know the difference.

"""

# ------------------------------ Example 1 -------------------------------------

# ------------------------------ Good -------------------------------------

from abc import ABC, abstractmethod


class Vehicle(ABC):

    @abstractmethod
    def get_max_speed(self):
        pass

    @abstractmethod
    def get_capacity(self):
        pass


class Car(Vehicle):

    def get_max_speed(self):
        pass

    def get_capacity(self):
        pass

    def is_hatchback(self):
        pass


class Bus(Vehicle):

    def get_max_speed(self):
        pass

    def get_capacity(self):
        pass

    def get_emergency_exit_location(self):
        pass


vehicle = Car()
vehicle.get_max_speed()  # This will work, without user knowing any difference
vehicle = Bus()
vehicle.get_capacity()  # This will work as well


# ------------------------------ BAD -------------------------------------


class Rectangle:

    def __init__(self, height, width):
        self.height = height
        self.width = width

    def set_height(self, height):
        self.height = height

    def set_width(self, width):
        self.width = width


class Square(Rectangle):

    def __init__(self, length):
        super().__init__(height=length, width=length)

    def set_height(self, length):
        self.height = length
        self.width = length

    def set_width(self, length):
        self.height = length
        self.width = length


shape = Rectangle(5, 10)
shape.set_height(10)  # Sets the height perfectly as expected
shape = Square(5)
shape.set_height(10)  # Sets the height perfectly, but but but, also changes the width of the shape,
# This the user can clearly see the difference
