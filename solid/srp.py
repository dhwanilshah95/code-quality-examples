"""
SRP - Single Responsibility Principle

A class should have one and only one reason to change.
Reason here is not a feature or functionality.
Reason means a business requirements or who is the user of the class.
"""


# ------------------------------ Example 1 -------------------------------------

# ------------------------------ DON'T -------------------------------------


class Employee:

    def update(self):
        """
        Updates employee details in database
        """
        pass

    def generate_report(self):
        """
        Generates report based on the employee's performance
        """
        pass

    def calculate_salary(self):
        """
        Calculates Salary of the employee
        """
        pass


# --------------------------------- DO ------------------------------------

class Employee:

    def update(self):
        """
        Updates employee details in database
        """
        pass


class PerformanceReport:

    def __init__(self, employee):
        self.employee = employee

    def generate(self):
        """
        Generates report based on the employee's performance
        """
        pass


class Salary:
    def __init__(self, employee):
        self.employee = employee

    def calculate(self):
        """
        Calculates Salary of the employee
        """
        pass


# ------------------------------ Example 2 -------------------------------------

# ------------------------------ DON'T -------------------------------------

class Report:

    def generate(self):
        # Initiates generation of the report
        pass

    def get_change(self):
        # One of the N helper methods that returns some calculation
        pass

    def export(self, _format):
        """
        :param _format: Format of report - PDF, Email, PPTX, DOCX
        :return: Exports report in provided format
        """
        pass


# ------------------------------ DO -------------------------------------

class Report:

    def generate(self):
        # Still initiates generation of the report
        pass

    def get_change(self):
        # Still one of the N helper methods that returns some calculation
        pass


class ReportExporter:

    def __init__(self, report):
        self.report = report

    def export(self, _format):
        try:
            factory = {
                'pdf': self.export_pdf,
                'email': self.send_email,
                'pptx': self.export_pptx,
                'docx': self.export_docx,
            }
            factory[_format]()
        except KeyError:
            raise NotImplementedError(f'{_format} export for a report is not supported yet.')

    def export_pdf(self):
        pass

    def send_email(self):
        pass

    def export_pptx(self):
        pass

    def export_docx(self):
        pass
