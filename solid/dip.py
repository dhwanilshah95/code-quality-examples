"""
DIP - Dependency Inversion Principle

Entities must depend on abstractions not on concretions. It states that the high level module
 must not depend on the low level module, but they should depend on abstractions.

"""


# ------------------------------ Example 1 -------------------------------------

# ------------------------------ DON'T -------------------------------------

class PostgresDatabase:

    def connect(self):
        pass

    def get_tables(self):
        pass


def get_tables(database: PostgresDatabase):
    """
    A method that client interacts with to get a list of all tables
    :param database: Postgres database object
    :return: tables from that database
    """
    database.connect()
    return database.get_tables()


# ------------------------------ DO -------------------------------------
from abc import ABC, abstractmethod


class Database(ABC):

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def get_tables(self):
        pass


class PostgresDatabase(Database):

    def connect(self):
        pass

    def get_tables(self):
        pass


def get_tables(database: Database):
    """
    A method that client interacts with to get a list of all tables
    :param database: An object which is a sub-class of Database class
    :return: tables from that database
    """
    database.connect()
    return database.get_tables()
