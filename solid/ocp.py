"""
OCP - Open/Closed Principle

A class should always be open for extension and closed for modification.
A high level policy should not be concerned about low-level details
"""


# ------------------------------ Example 1 -------------------------------------

# ------------------------------ DON'T -------------------------------------

class Shape:

    def __init__(self, _type):
        self.type = _type


class Circle(Shape):

    def __init__(self, radius):
        self.radius = radius
        super().__init__('circle')

    def draw_circle(self):
        pass


class Square(Shape):

    def __init__(self, length):
        self.length = length
        super().__init__('square')

    def draw_square(self):
        pass

    def rotate_square(self, degree):
        pass


class Rectangle(Shape):

    def __init__(self, height, width):
        self.height = height
        self.width = width
        super().__init__('rectangle')

    def draw_rectangle(self):
        pass

    def rotate_rectangle(self, degree):
        pass


class Plotter:

    def __init__(self, shapes):
        self.shapes = shapes

    def draw(self):
        for shape in self.shapes:
            if shape.type == 'circle':
                shape.draw_circle()
            elif shape.type == 'square':
                shape.draw_square()
            elif shape.type == 'rectangle':
                shape.draw_rectangle()

    def rotate(self, degree):
        for shape in self.shapes:
            if shape.type == 'rectangle':
                shape.rotate_rectangle(degree)
            elif shape.type == 'square':
                shape.rotate_square(degree)


# ------------------------------ DO -------------------------------------

from abc import ABC, abstractmethod


class Shape(ABC):

    def __init__(self, _type):
        self.type = _type

    @abstractmethod
    def draw(self):
        pass

    @abstractmethod
    def rotate(self, degree):
        pass


class Circle(Shape):

    def __init__(self, radius):
        self.radius = radius
        super().__init__('circle')

    def draw(self):
        # Plot a circle
        pass

    def rotate(self, **kwargs):
        # Literally just pass because there is no point in rotating a circle
        pass


class Square(Shape):

    def __init__(self, length):
        self.length = length
        super().__init__('square')

    def draw(self):
        # Plot a circle
        pass

    def rotate(self, degree):
        # Rotate the square by x degrees
        pass


class Rectangle(Shape):

    def __init__(self, height, width):
        self.height = height
        self.width = width
        super().__init__('rectangle')

    def draw(self):
        pass

    def rotate(self, degree):
        pass


class Plotter:

    def __init__(self, shapes):
        self.shapes = shapes

    def draw(self):
        for shape in self.shapes:
            shape.draw()

    def rotate(self, degree):
        for shape in self.shapes:
            shape.rotate(degree)
